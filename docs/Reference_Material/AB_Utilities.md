# AbettorBot Utilities File

Rather than keeping functions in every cog that requires them, any generalized classes/functions are now going to exist in the `ab_utils.py` file.

To import these functions, use `from ab_utils import xyz`, where `xyz` is the name of the class/function to import for use.

Items added in `ab_utils` include:

- admincheck
- mkdir_p
- Confirm(), a class for adding Confirm/Cancel buttons with a discord View object
