# AbettorBot Public Repository

AbettorBot's latest documentation can be viewed [here](https://abettorbot.gitlab.io/abettorbot). The original blog site can be viewed [here](https://abettorbot.home.blog/).

If you feel so inclined, you can support AbettorBot via Ko-Fi :heart:

<a href='https://ko-fi.com/rwarcards762' target='_blank'><img height='35' style='border:0px;height:46px;' src='https://az743702.vo.msecnd.net/cdn/kofi3.png?v=0' border='0' alt='Buy Me a Coffee at ko-fi.com' />

## What is this repository for?

This is the GitLab Public Repository for the currently closed-source AbettorBot Discord Bot. The code will become open-source on release of AbettorBot 3 (v2.0.0)!

The bot currently uses the latest build of Pycord (v2.0.1) and is written in Python 3.10.

Initially, this repository will exist to host a copy of the documentation, moving away from the [original website/blog](https://abettorbot.home.blog/) as I find it clunky and a pain to maintain. Upon release of AB3-Slash ("AbettorBot 2"), this will be the bot's home where updates and patches roll out! Users can submit Issues for bugs they find, submit Feature Requests, and more!

## Features

TBA
